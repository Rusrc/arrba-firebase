import * as functions from 'firebase-functions';
import { Storage } from '@google-cloud/storage';
import { tmpdir } from 'os';
import { join, dirname, basename } from 'path';

import * as sharp from 'sharp';
import * as fs from 'fs-extra';
// import * as admin from 'firebase-admin';

// admin.initializeApp();
sharp.cache({ files: 0 });

const gsc = new Storage();

export const authernticationChech = functions.https.onCall((data, context) => {
    console.log('context: ', context);
    console.log('data: ', data);
});

/**
 *  const BIG_FILE_NAME_PREFIX = "-1920x1080";
 *  const FULL_FILE_NAME_PREFIX = "-1024x720";
 *  const MIDDLE_FILE_NAME_PREFIX = "-400x300";
 *  const MIDDLE_FILE_NAME_PREFIX2 = "-550x412";
 *  const SMALL_FILE_NAME_PREFIX = "-200x150";
 *  const SMALL_MIDDLE_FILE_NAME_PREFIX = "-120x90";
 *  const SUPER_SMALL_FILE_NAME_PREFIX = "-60x45";
 */
export const uploadAndProcessImage = functions.runWith({
    timeoutSeconds: 300, // seconds, increase the timeout for long-running function to avoid memory overload
    memory: '512MB' // 128MB, 256MB, 512MB, 1GB, 2GB
}).storage.object().onFinalize(async data => {

    const bucket = gsc.bucket(data.bucket)

    const filePath = data.name || '';
    const fileName = basename(filePath);
    const contentType = data.contentType || '';
    const bucketDir = dirname(filePath);

    if (!filePath || !fileName || !contentType) {
        throw Error(`Variable emptry. filePath: "${filePath}", fileName: "${fileName}", contentType: "${contentType}"`);
    }

    const workingDir = join(tmpdir(), 'thumbs');
    const tmpFilePath = join(workingDir, fileName);

    if (fileName.includes('thumb@') || !contentType.includes('image')) {
        return false;
    };

    await fs.ensureDir(workingDir);

    await bucket.file(filePath).download({
        destination: tmpFilePath
    });

    const sizes = [
        // images_thumb@1920x1080-0dafbb8e-e77a-4f94-8ff9-a1982ea702ab_1.jpg
        { width: 1920, height: 1080, main: true }, 
        { width: 540, height: 405, main: false },
    ];

    const uploadPromises = sizes.map(async size => {

        let thumbName = '';
        if (size.main) {
            thumbName = `thumb@${fileName}`;
        } else {
            thumbName = `thumb@${size.width}x${size.height}-${fileName}`;
        }

        // Make lowe case to prevent linux sensitive path.
        thumbName = thumbName.toLowerCase(); 

        const thumbPath = join(workingDir, thumbName);

        await sharp(tmpFilePath)
            .resize(size.width, size.height)
            .toFile(thumbPath);

        if (global.gc) global.gc();

        // Upload to GCS
        return bucket.upload(thumbPath, {
            destination: join(bucketDir, thumbName)
        });
    });


    //4. Run the upload operations
    await Promise.all(uploadPromises);

    // 5 delete original file, but it could be deteted from any sub folder only. E.g. images/
    const originalFilePath = join(bucketDir, fileName);
    await bucket.file(originalFilePath).delete();

    // await sharp(join(bucketDir, fileName)).resize(1920, 1080).toFile(join(bucketDir, fileName));
    // 6. Cleanup remove the tmp/thumbs from filessytem
    return fs.remove(workingDir);
});